﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;

namespace CalculatorTest
{
    [TestClass]
    public class UnitTest1
    {
        static WindowsDriver<WindowsElement> sessionCalc;

        [ClassInitialize]
        public static void PrepareForTestingAlarms(TestContext tc)
        {
            AppiumOptions desireCapabilities = new AppiumOptions();
            desireCapabilities.AddAdditionalCapability("app", @"C:\Users\ssele\source\repos\CalculatorTest\Calculator\Calculator\bin\Debug\Calculator");
            sessionCalc = new WindowsDriver<WindowsElement>(new Uri("http://127.0.0.1:4723"), desireCapabilities);
        }


        [TestMethod]
        public void VerifyAddition()
        {
            var btnTwo = sessionCalc.FindElementByName("2");
            var btnThree = sessionCalc.FindElementByName("3");

            btnTwo.Click();

            sessionCalc.FindElementByName("+").Click();

            btnThree.Click();

            sessionCalc.FindElementByName("=").Click();

            var calculatorResults = sessionCalc.FindElementByAccessibilityId("textBox_Result");

            if (calculatorResults.Text.Contains("6"))
            {
                Console.WriteLine("The result is correct");
            }
            else
            {
                Console.WriteLine("The result is incorrect");
            }

        }


        [ClassCleanup]
        public static void CleanupAllAlarmsTests()
        {
            if(sessionCalc != null)
            {
                sessionCalc.Quit();
            }
        }
    }
}
